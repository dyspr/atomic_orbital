var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var dimension = 7
var initSize = 1
var steps = 128

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < Math.PI * 2; i += (Math.PI * 2 / steps)) {
    for (var j = 0; j < dimension; j++) {
      fill(128 + 128 * Math.random())
      noStroke()
      ellipse(windowWidth * 0.5 + boardSize * 0.4 * initSize * (1 - (j + 1) / dimension) * sin(i + (frameCount * 0.125) + j) + Math.random() * boardSize * 0.02 - boardSize * 0.01, windowHeight * 0.5 + boardSize * 0.4 * initSize * (1 - (j + 1) / dimension) * cos(i + (frameCount * 0.0625) - j) + Math.random() * boardSize * 0.02 - boardSize * 0.01, boardSize * 0.0125 * ((dimension - j + 1) / dimension))

      ellipse(windowWidth * 0.5 + boardSize * 0.4 * initSize * (1 - (j + 1) / dimension) * sin((Math.PI * 2 - j) + (frameCount * 0.125) + j), windowHeight * 0.5 + boardSize * 0.4 * initSize * (1 - (j + 1) / dimension) * cos((Math.PI * 2 - j) + (frameCount * 0.0625) - j), boardSize * 0.05 * ((dimension - j + 1) / dimension))
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
